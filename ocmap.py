#!/usr/bin/env python3
#
#   Folium map for occultation observation.
#
# Revision History:
"""
 Ver. 0.20: 2022-12-11; Add options. Line color/width modified.
 Ver. 0.21: 2022-12-13; Options added. (boundary, line ranges, etc.)
 Ver. 0.22: 2022-12-16; Add LocateControl, read topological kml file.
 Ver. 0.23: 2022-12-30; Read observers. File name changed (gsimap_test.py -> ocmap.py)
 Ver. 0.24: 2023-01-12; Draw arbitrary fences, etc.
 Ver. 0.25: 2023-01-15; Marker style key, 'fill' added.
 Ver. 0.26: 2023-01-16; Bug fix (boundary box), mini map added.
 Ver. 0.27: 2023-01-22; Bug fix (remove redundant maps), skip invalid observer entry.
 Ver. 0.28: 2023-03-07; Temporal option for 2023-03-09 event. (--temp-20230309 option)
 Ver. 0.29: 2023-04-28; Multiple data files input.
 Ver. 0.29b: 2023-04-29; Add line width selector.
 Ver. 0.30: 2023-06-05; Debug for directory extract for csv and kml files.
 Ver. 0.31beta: 2023-10/05; Add large star mode (alpha Ori).
"""

__version__ = '0.31beta'
__author__ = 'Hiroshi Akitaya'

import json
import os
import sys
import csv
import glob

import folium
import numpy as np
import requests
from folium import FeatureGroup, LayerControl
from folium import plugins
from folium.features import DivIcon
from folium.features import LatLngPopup
from jinja2 import Template
from numpy import ndarray

# LOC_CENTER = [34.649441, 135.001304]  # Center coordinate.
_LOC_CENTER_DEFAULT = (34.043557, 132.846680)  # CC2021-2023-02-06
_WIDTH_DEFAULT = 800
_HEIGHT_DEFAULT = 600
_ZOOM_DEFAULT = 8
_LEN_OBSERVER = 4  # Length of observer csv line.

FUNCTIONS_JS = './functions.js'  # JavaScript functions to be imported.
URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png'
# URL_BASE = 'OpenStreetMap'
URL_MAP1 = 'https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg'
ATTR_DEFAULT = 'GSI'


class FoliumMap(object):
    lw_prdct = 5  # Width of predict lines (px).

    # Definition of marker style for observers.
    observer_marker_style = {
        '1': {'color': '#ff0000', 'radius': 10, 'fill': True},
        '2': {'color': '#0000ff', 'radius': 10, 'fill': True},
        '3': {'color': '#ff0000', 'radius': 10, 'fill': False},
        '4': {'color': '#0000ff', 'radius': 10, 'fill': False},
    }
    xml_namespace = '{http://www.opengis.net/kml/2.2}'  # Default XML namespace.

    def __init__(self, location=_LOC_CENTER_DEFAULT, width=_WIDTH_DEFAULT, height=_HEIGHT_DEFAULT,
                 zoom_start=_ZOOM_DEFAULT) -> None:
        self.gj = None
        self.m = folium.Map(tiles=None, location=location, width=width, height=height,
                            control_scale=True, zoom_start=zoom_start)
        # Read Javascript file.
        self.m.get_root().html.add_child(folium.JavascriptLink(FUNCTIONS_JS))
        self.groups = {}
        self.m.add_child(GetLatLngPopup())
        # self.jg = None  # Geojson values.  # Probably not used.
        self.debug_mode = False

    @staticmethod
    def set_xml_namespace(namespace_str: str):
        """ Set xml namespace string.
        """
        FoliumMap.xml_namespace = namespace_str

    def set_debug_mode(self, flag=True):
        """Debug mode on.
        :param flag: Debug mode on (True: default) or off (False).
        :type flag: bool
        """
        self.debug_mode = flag
        return flag

    def add_tile_layers(self):
        """Add tile layers to the map instance.
        """

        # GSI standard map.
        folium.TileLayer(
            tiles=URL_BASE,
            name=u'国土地理院 標準地図',
            attr=u'<a href="https://www.gsi.go.jp/">国土地理院</a>',
        ).add_to(self.m)

        # GSI photo layers.
        folium.TileLayer(
            # GSI photos.
            tiles='https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg',
            name=u'国土地理院 最新航空写真',
            attr=u'Data by <a href="https://www.gsi.go.jp/">国土地理院</a>',
        ).add_to(self.m)

        # GSI color elevation map.
        # https://maps.gsi.go.jp/development/ichiran.html#relief
        folium.TileLayer(
            tiles='https://cyberjapandata.gsi.go.jp/xyz/relief/{z}/{x}/{y}.png',
            name=u'国土地理院 色別標高図',
            attr='<a href="https://www.gsi.go.jp/">国土地理院</a>  (海域部は海上保安庁海洋情報部の資料を使用して作成)',
        ).add_to(self.m)

        # OpenStreet Map.
        folium.TileLayer(
            tiles='openstreetmap',
        ).add_to(self.m)

        # Stamen Terrain.
        folium.TileLayer(
            tiles='Stamen Terrain',
        ).add_to(self.m)

    def add_locate_control(self):
        """Add Locale Control Plugin button."""
        plugins.LocateControl().add_to(self.m)

    def add_layer_control(self):
        """Add Layer Control Plugin button."""
        LayerControl().add_to(self.m)  # Layer control.

    def add_measure_control(self):
        """Add Measure Control Plugin button."""
        plugins.MeasureControl().add_to(self.m)

    def add_minimap(self):
        """Add Mini map Plugin."""
        plugins.MiniMap(toggle_display=True).add_to(self.m)

    def add_fullscreen(self):
        """Add Fullscreen Plugin button. """
        plugins.Fullscreen(
            position="topright", title="Fullscreen", title_cancel="Back",
        ).add_to(self.m)

    def make_group(self, name_group):
        """Make layer group. """
        self.groups[name_group] = FeatureGroup(name=name_group)

    def add_groups_to_map(self):
        """Add layer groups to the map instance."""
        for group in self.groups.values():
            group.add_to(self.m)

    def add_map_to_group(self, url: str, group: str):
        """Add map to the group. """
        folium.Map(tiles=url, attr='GSI').add_to(self.groups[group])

    def _get_map_instance(self, group):
        if (group is None) or (group not in self.groups):
            return self.m
        else:
            return self.groups[group]

    def add_observers_markers(self, observers: list, group: str = None):
        """ Add observers (in the list) to the layer group. """
        print(observers)
        for observer in observers:
            if len(observer) < _LEN_OBSERVER:
                continue
            name = observer[0]
            try:
                coord = (float(observer[2]), float(observer[1]))  # Lat, Lng
            except ValueError:
                sys.stderr.write('Wrong Lng/Lat format.\n')
                continue
            # Skip invalid observers.
            if name == '' or (np.abs(coord[0]) < 1e-8 and np.abs(coord[1]) < 1e-8):
                continue
            observer_type = observer[3]
            if (observer_type in FoliumMap.observer_marker_style) is False:
                sys.stderr.write('Observer type {} not defined.\n'.format(observer_type))
                continue
            color = FoliumMap.observer_marker_style[observer_type]['color']
            radius = FoliumMap.observer_marker_style[observer_type]['radius']
            fill = FoliumMap.observer_marker_style[observer_type]['fill']
            self.add_marker(coord, popup_name=name, fill=fill, color=color,
                            tooltip=name, radius=radius, group=group)

    def add_marker(self, coord: tuple, popup_name: str, fill: bool,
                   color: str, tooltip: str = None,
                   radius: float = 10.0, group: str = None):
        """
        :list coord:
        :str popup_name:
        :bool fill: Fill marker (True or False).
        :str color:
        :str tooltip:
        :float radius:
        :bool group:
        """
        coord_str = '<a href="http://www.google.com/maps/place/{0:},{1:}" target="_blank" title="Google Map">{0:.6f},' \
                    '{1:.6f}</a>'.format(coord[0], coord[1])
        elevation_str = 'Elevation: <a href="{}" target="_blank">GSI API</a>'.format(get_gsi_elevation_api_url(coord))
        # Supress vertical style for Japanese letters.
        # Ref.: https://qiita.com/yuta_muds/items/1a4762ae0a9aaa4c783d
        popup = ''
        popup += '<span style="white-space: nowrap;">' + \
                 'Registered Point</br>' + \
                 popup_name + '</br>' + coord_str + '</br>' + elevation_str + \
                 '<span id="elevation"></span>'
        # popup += '<script>get_gsi_elevation({}, {});</script>'.format(coord[0], coord[1])
        folium.CircleMarker(coord, radius=radius, popup=popup, fill=fill, fill_opacity=1.0,
                            color=color, tooltip=tooltip).add_to(
            self._get_map_instance(group)
        )

    def add_string_marker(self, coord, text, color='#000000', group=None):
        folium.Marker(
            coord,
            icon=DivIcon(
                icon_size=(250, 36),
                icon_anchor=(0, 0),
                html='<div style="font-size: 10pt; color: {}" alt="{}">{}</div>'.format(color, text, text)
                # html = '<div>{}</div>'.format(text)
            )
        ).add_to(self._get_map_instance(group))

    def add_markers(self, mymarkers):
        for mk in mymarkers:
            self.add_marker(mk.coord, mk.observer, mk.fill, mk.color)

    def add_polyline(self, coords_in, color='#0000ff', tooltip=None, weight=1, line_weight=1, group=None):
        folium.PolyLine(locations=coords_in, tooltip=tooltip, wcwidth=weight,
                        weight=line_weight,
                        color=color).add_to(self._get_map_instance(group))
        # print('Adding polyline'.format())

    def save_html(self, fn: str):
        """ Save html file with Leaflet JavaScript codes.
        str fn: Output file name (html).
        """
        try:
            self.m.save(fn)
        except OSError:
            sys.stderr.write('File output failed: {}'.format(fn))
            sys.exit(1)

    def add_json(self, fn_json: str):
        # print(fn_json)
        folium.TopoJson(
            json.loads(requests.get(fn_json).text),
            "objects.antarctic_ice_shelf",
            name="topojson",
        ).add_to(self.m)
        folium.LayerControl().add_to(self.m)

    def read_geojson(self, fn):
        with open(fn, 'r') as f:
            self.gj = json.load(f)
        return 0

    def get_geojson_linecoords(self):
        _i = 0
        for item in self.gj['features']:
            _i += 1
            if item['geometry']['type'] == 'LineString':
                if _i < 4:
                    continue
                coordinates = item['geometry']['coordinates']
                return coordinates_reverse(coordinates)

    def draw_geojson_seplines(self, seps, coords_in):
        for sep in seps:
            coords_sep = calc_separated_coords(sep, coords_in)
            # print(coords_sep)
            self.add_polyline(coords_sep)

    def draw_fences(self, coords_in, index_fence, interval_fence, group, interval=5, factor=1.0,
                    color_usual='#ff9933', color_em='#ff3300'):
        """ Draw observation fences.
        :list coords_in:
        :list seps:
        :str group:
        """
        # l0_index = _calc_l0_index(indx_fence)
        for indx in index_fence:
            _line_color = color_em if (indx % interval) == 0 else color_usual
            coord_new = calc_separated_coords(indx * interval_fence * factor, coords_in)
            self.add_polyline(coord_new, color=_line_color,
                              tooltip='F{:+d} Fence @ ({:.0f} m)<br /> '
                                      'w/o altitude corrected'.format(int(indx), indx * interval_fence * 1000),
                              group=group)
            # Obs. line label.
            for j in np.linspace(0, len(coord_new) - 1, interval):
                j = int(j)
                self.add_string_marker(coord_new[j], 'F{:+d}'.format(int(indx)), color=_line_color, group=group)

    def draw_geojson_linestring(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'LineString':
                coordinates = item['geometry']['coordinates']
                self.add_polyline(coordinates_reverse(coordinates), group=group)

    def draw_prediction_lines(self, lines: list, labels: list, group=None, color_def=None, no_sigma=False):
        if color_def is None:
            color_def = {'center': 'red', 'boundary': 'blue', 'sigma1': 'green', 'penumb': 'pink',
                         'umbral': 'cyan'}
        if len(lines) < 5:
            return 1

        for label in labels:
            idx = labels.index(label)
            if label == 'Center Line':
                self.add_polyline(lines[idx], color=color_def['center'], tooltip='Prediction Center',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif label == 'Path limit right':
                self.add_polyline(lines[idx], color=color_def['boundary'], tooltip='Prediction Boundary (+)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif label == 'Path limit left':
                self.add_polyline(lines[idx], color=color_def['boundary'], tooltip='Prediction Boundary (-)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)

            elif (label == '1-sigma right') and (not no_sigma):
                self.add_polyline(lines[idx], color=color_def['sigma1'], tooltip='Prediction 1-sigma (+)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif (label == '1-sigma left') and (not no_sigma):
                self.add_polyline(lines[idx], color=color_def['sigma1'], tooltip='Prediction 1-sigma (-)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)

            # Additional prediction lines for partial occultation of a large star (such as alpha Ori).
            # 2023/10/16 HA
            elif label == 'Penumbral limit right':
                self.add_polyline(lines[idx], color=color_def['penumb'], tooltip='Penumbral Limit (+)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif label == 'Penumbral limit left':
                self.add_polyline(lines[idx], color=color_def['penumb'], tooltip='Penumbral Limit (-)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif label == 'Umbral limit right':
                self.add_polyline(lines[idx], color=color_def['umbral'], tooltip='Umbral Limit (+)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)
            elif label == 'Umbral limit left':
                self.add_polyline(lines[idx], color=color_def['umbral'], tooltip='Umbral Limit (-)',
                                  weight=FoliumMap.lw_prdct, line_weight=FoliumMap.lw_prdct,
                                  group=group)

    def draw_geojson_point(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'Point':
                coordinate = item['geometry']['coordinates']
                coordinate.reverse()
                self.add_marker(coordinate, item['properties']['name'],
                                True, '#ff0000', tooltip=item['properties']['name'],
                                group=group)

    def draw_geojson_diagrams(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'LineString':
                coordinates = item['geometry']['coordinates']
                self.add_polyline(coordinates_reverse(coordinates), group=group)
            elif item['geometry']['type'] == 'Point':
                coordinate = item['geometry']['coordinates']
                coordinate.reverse()
                self.add_marker(coordinate, item['properties']['name'],
                                True, '#ff0000', group=group)

    def draw(self):
        folium.LayerControl().add_to(self.m)


def set_line_width(lw):
    """
    Set line width for folium map.
    :param lw: Line width.
    :return:
    """
    FoliumMap.lw_prdct = lw


def define_observer_marker_style(style_csv_fn, debug_mode=False):
    with open(style_csv_fn, 'r', encoding='utf-8-sig') as f:
        # reader = csv.reader(f)
        reader = csv.reader(filter(lambda _row: _row[0] != '#', f))  # Skip csv comment line (#...).

        for row in reader:
            if debug_mode:
                print(row)
            try:
                FoliumMap.observer_marker_style[row[0]] = {'color': row[1],
                                                           'radius': float(row[2]),
                                                           'fill': True if (int(row[3]) == 1) else False
                                                           }
            except ValueError:
                sys.stderr.write('Wrong marker style.\n')
    if debug_mode:
        print(FoliumMap.observer_marker_style)


class MyMarker(object):
    def __init__(self, coord, observer, fill=True, color=''):
        self.coord = coord
        self.observer = observer
        self.fill = fill
        self.color = color


def read_observers_csv(fn_in) -> list:
    """Read observer CSV file.
    UTF-8 encode is expected.
    """
    with open(fn_in, 'r', encoding='utf-8-sig') as f:
        # reader = csv.reader(f)
        reader = csv.reader(filter(lambda _row: _row[0] != '#', f))  # Skip csv comment line (#...).
        print(reader)

        try:
            observers = [row for row in reader]
        except UnicodeDecodeError:
            sys.stderr.write('Encode error in {}. UTF-8 is expected.\n'.format(fn_in))
            sys.exit(1)
        observers.pop(0)
    return observers


def read_occult_kml_fences(fn: str) -> list:
    """
    OCCULT kml file reader.
    :rtype: object
    :param fn:
    :return:
    """
    import xml.etree.ElementTree as ETtree
    _lines = []  # Result list [[linename, coords list], ...}

    # Extract kml file from kmz file
    import zipfile
    if zipfile.is_zipfile(fn):
        with zipfile.ZipFile(fn, 'r') as zf:
            fns = zf.namelist()
            kml_fns = [fn if (os.path.splitext(fn)[1] == '.kml') else None for fn in fns]
            for fn_tmp in fns:
                if os.path.splitext(fn_tmp)[1] == '.kml':
                    root = ETtree.fromstring(zf.read(fn_tmp))
                    print(fn_tmp)
    else:
        try:
            tree = ETtree.parse(fn)
        except ETtree.ParseError:
            sys.stderr.write('XML parse error.\n')
            sys.exit(1)
        except OSError:
            sys.stderr.write('File reading error.\n')
            sys.exit(1)

        root = tree.getroot()

    # Find Placemark tags and read line information from the child tags and attributions.
    for child in root[0].iter('{}Placemark'.format(FoliumMap.xml_namespace)):
        line_label = None
        coords_str = None
        for child2 in child:
            if child2.tag.endswith('name') or child2.tag == 'name':
                line_label = child2.text
            elif child2.tag.endswith('LineString') or child2.tag == 'LineString':
                for child3 in child2:
                    if child3.tag.endswith('coordinates') or child3.tag == 'coordinates':
                        coords_str = child3.text
        if coords_str is not None:
            _lines.append([line_label, np.array(split_xyz_coords(coords_str.split()))])
    return _lines


def get_topographic_corrected_occult_kml(fn: str, verbose=False) -> list:
    """
    Get Topographic Corrected fences from OCCULT kml file.
    :param fn: input file name.
    :param verbose: flag verose mode.
    :return: Coordinates list.
    """
    import xml.etree.ElementTree
    n_lines_found = 0
    lines = []
    tree = xml.etree.ElementTree.parse(fn)
    root = tree.getroot()

    for child in root[0]:
        if child.tag.endswith('Placemark'):
            coords = child[2][2].text.split()
            lines.append(np.array(split_xyz_coords(coords)))
            n_lines_found += 1
    if verbose:
        print('Number of lines found: {}'.format(n_lines_found))
    return lines


def get_prediction_lines_from_occult_kml(fn: str, verbose=False) -> list:
    """
    Get prediction lines (5 lines) form Occult prediction kml file.
    """
    import xml.etree.ElementTree
    n_lines_found = 0
    lines = []
    tree = xml.etree.ElementTree.parse(fn)
    root = tree.getroot()
    for linestring in root.iter():
        if linestring.tag != 'coordinates':
            continue
        coords = linestring.text.split()
        lines.append(np.array(split_xyz_coords(coords)))
        n_lines_found += 1
    if verbose:
        print('Number of lines found: {}'.format(n_lines_found))
    return lines


def split_xyz_coords(coords: list) -> list:
    """
    Convert (lat, lng, height) coordinates strings
    into (lng, lat) coordinates.
    :list coords: Coordinates; list of (Longitude, Latitude).
    :list: List of (Longitude, Latitude) coordinates.
    """
    coords_xy: list[list[float]] = []  # Initialize coordinate list.
    for coord in coords:
        if len(coord) < 2:
            raise ValueError
        _vals = coord.split(',')
        try:
            coords_xy.append([float(_vals[1]), float(_vals[0])])  # Append [lng, lat].
        except ValueError:
            pass
    return coords_xy


def _calc_l0_index(seps: list) -> int:
    """
    Calculate central index with zero value from the discrete list at even interval.
    :seps: Values list.
    :return: Zero-value index.
    """
    return int((-1.0) * seps[0] * (len(seps) - 1) / (seps[-1] - seps[0]))


def cut_line_in_area(line: np.ndarray, lng_min: float, lng_max: float,
                     lat_min: float, lat_max: float) -> np.ndarray:
    """ Cut out line coordinates (2-d numpy.ndarray) within
    [lng_min:lng_max, lat_min:lat_max] coordinate window.
    """
    lt = line.T
    indx = np.intersect1d(np.where((lt[1] > lng_min) & (lt[1] < lng_max)),
                          np.where((lt[0] > lat_min) & (lt[0] < lat_max))
                          )
    newline = line[indx]
    return newline


def calc_separated_latlng(sep: float,
                          lat_deg: float, lng_deg: float,
                          d_lat_deg: float, d_lng_deg: float) -> (float, float):
    """ Calculate a new lat-lng coordinate with a distance of sep km.
    """
    r_earth = 6371.0  # km
    # R_EARTH = 5660.0  # km
    psi = np.arctan(d_lat_deg / (d_lng_deg * np.cos(np.deg2rad(lat_deg))))  # PA of the reference vector.
    # psi = np.arctan2(d_lat_deg, d_lng_deg)  # PA of the reference vector.
    psi_perp = psi + np.pi / 2  # PA perpendicular to the ref. vector.
    # print(np.rad2deg(psi_perp))
    sep_lat_deg = np.rad2deg(sep / r_earth * np.sin(psi_perp))  # New latitude.
    sep_lng_deg = np.rad2deg(sep / r_earth * np.cos(psi_perp) / np.cos(np.deg2rad(lat_deg)))  # New longitude.
    return lat_deg + sep_lat_deg, lng_deg + sep_lng_deg


def calc_separated_coords(sep: float, coords_in: list) -> list:
    # print(sep)
    coords_sep = []
    coord_pre = None
    for coord in coords_in:
        if coord_pre is None:
            coord_pre = coord
            continue
        lat, lng = coord
        lat_pre, lng_pre = coord_pre
        coords_sep.append(calc_separated_latlng(sep, lat, lng, lat - lat_pre, lng - lng_pre))
        coord_pre = coord
    return coords_sep


def coordinates_reverse(coords_in):
    coords_rev = []
    for coord in coords_in:
        coords_rev.append([coord[1], coord[0]])
    return coords_rev


def get_gsi_elevation_api_url(coord):
    # Ref. https://www.gis-py.com/entry/elevation-api
    # https://maps.gsi.go.jp/development/elevation_s.html
    lon = coord[1]
    lat = coord[0]
    url = 'http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon={}&lat={}'.format(lon, lat)
    return url


def get_lines_from_occult_file(fn, debug=False):
    #coords_prediction = [[], [], [], [], []]
    #labels_prediction = [[], [], [], [], []]
    coords_prediction = []
    labels_prediction = []
    coords_observation = []
    try:
        _fences = read_occult_kml_fences(fn)
    except OSError:
        sys.stderr.write('Error. Failed to open {}.'.format(fn))
        return coords_prediction, labels_prediction, coords_observation
    if debug: print(f'[kml fences] {len(_fences)}')

    # Keys in kml file (<name> ...</name>) for prediction lines.
    keys_prediction = ['Center line', 'Path limit right', 'Path limit left', '1-sigma right',
                       '1-sigma left',
                       'Penumbral limit left', 'Penumbral limit right',
                       'Umbral limit left', 'Umbral limit right',
    ]
    for _vals in _fences:
        _label = _vals[0]
        _coords = _vals[1]
        print(f'[DEBUG] "{_label}", "{_coords}"')
        if _label in keys_prediction:
            coords_prediction.append(_coords)
            labels_prediction.append(_label)

        #if _label == 'Center line':
        #    coords_prediction[0] = _coords
        #    labels_prediction[0] = _label
        #elif _label == 'Path limit right':
        #    coords_prediction[1] = _coords
        #    labels_prediction[1] = _label
        #elif _label == 'Path limit left':
        #    coords_prediction[2] = _coords
        #    labels_prediction[2] = _label
        #elif _label == '1-sigma right':
        #    coords_prediction[3] = _coords
        #    labels_prediction[3] = _label
        #elif _label == '1-sigma left':
        #    coords_prediction[4] = _coords
        #    labels_prediction[4] = _label

        else:
            coords_observation.append([_label, _coords])
        if debug: print(f'[DEBUG] {labels_prediction}')
    return coords_prediction, labels_prediction, coords_observation


# Emergent process for 2023/3/9 Chugoku-area event.
# 2023/03/07
def coords_correct_20230309_chugoku(_coords_prediction, _coords_observation):
    print(len(_coords_observation))
    _coords_observation.insert(17, ['Fence @ 300 m right', _coords_prediction[1].copy()])
    _coords_observation.insert(22, ['Fence @ 300 m left', _coords_prediction[2].copy()])
    print(len(_coords_observation))
    return _coords_observation


def get_filenames_from_list_and_dir(flist, ext=""):
    fns_result = []
    for fn in flist:
        if os.path.isdir(fn):
            fns_result.extend(glob.glob(f'{fn}/*{ext}'))
        else:
            fns_result.append(fn)
    return fns_result


# Redefine LatLngPopup with higher decimal precision.
#  Ref. https://gis.stackexchange.com/questions/371628/get-coordinates-from-foliums-feature-latlngpopup-in-python
#
class GetLatLngPopup(LatLngPopup):
    _template = Template(u"""
            {% macro script(this, kwargs) %}
                var {{this.get_name()}} = L.popup();
                function latLngPop(e) {
                    {{this.get_name()}}
                        .setLatLng(e.latlng)
                        .setContent("<a href='http://www.google.com/maps/place/" +
                                e.latlng.lat.toFixed(6) + "," + e.latlng.lng.toFixed(6) + 
                                "' target='_blank' title='Google Map'>" + 
                                e.latlng.lat.toFixed(6) + "," + e.latlng.lng.toFixed(6) +
                                "</a><br />" +
                                "<a href='http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon=" +
                                e.latlng.lng.toFixed(6) +"&lat=" + e.latlng.lat.toFixed(6) + 
                                "' target='_blank' title='GSI API JSON file'>El.: </a>" +
                                "<span id='elevation'></span> m"
                                )
                        .openOn({{this._parent.get_name()}});
                        get_gsi_elevation(e.latlng.lat, e.latlng.lng);
                    }
                {{this._parent.get_name()}}.on('click', latLngPop);
            {% endmacro %}
            """)

    def __init__(self):
        super(GetLatLngPopup, self).__init__()
        self._name = 'GetLatLngPopup'


if __name__ == '__main__':
    _LOC_CENTER_DEFAULT = [34.043557, 132.846680]
    _RANGE_LNG_DEFAULT = [122.0, 154.0]
    _RANGE_LAT_DEFAULT = [20.0, 46.0]
    _RANGE_FENCE_DEFAULT = [-50, 50]
    _INTERVAL_FENCE_DEFAULT = 5
    _TICS_FENCE_DEFAULT = 5
    import argparse
    import webbrowser

    # Option analysis.
    parser = argparse.ArgumentParser()
    parser.add_argument('--width', default=800, type=int, help='map width [pixels]')
    parser.add_argument('--height', default=600, type=int, help='map height [pixels]')
    parser.add_argument('--zoom', default=8, type=int, help='initial zoom level [integer]')
    parser.add_argument('--center', nargs=2, default=_LOC_CENTER_DEFAULT, type=float,
                        help='center coordinate of the map (lng[deg lat[deg])')
    parser.add_argument('--range-lng', nargs=2, default=_RANGE_LNG_DEFAULT, type=float,
                        help='longitude range (min[deg] max[deg])')
    parser.add_argument('--range-lat', nargs=2, default=_RANGE_LAT_DEFAULT, type=float,
                        help='latitude range (min[deg] max[deg])')
    parser.add_argument('--with-fences', action='store_true',
                        help='Plot equidistant fences according to given parameters')
    parser.add_argument('--range-fence-num', '--range-line-num', '--rn', nargs=2, default=None, type=float,
                        help='number range of fences (Nmin Nmax)')
    parser.add_argument('--exclude-range-fence-num', '--exclude-range-line-num', '--exrn',
                        nargs=2, default=None, type=float,
                        help='exclude number range of fences (Nex_min Nex_max)')
    parser.add_argument('--range-fence', '--range-line', nargs=2, default=_RANGE_FENCE_DEFAULT, type=float,
                        help='range of fence (min[km] max[km])')
    parser.add_argument('--factor-fence-interval', default=1.0, type=float,
                        help='Factor to be multiplied to the fence interval (default: 1.0')
    parser.add_argument('--interval-fence', '--interval', type=float,
                        default=5.0, help='Interval between fences (default: 5.0) [km]')
    parser.add_argument('--tics-fence', '--tics-lines', default=_TICS_FENCE_DEFAULT, type=int,
                        help='color emphasis interval of fences [integer]')
    parser.add_argument('--zd', '--zenith-distance', default=0.0, type=float,
                        help='Zenith distance of the target [deg]')
    parser.add_argument('--apparent-pa', default=0.0, type=float,
                        help='Apparent moving PA of the asteroid [deg]')
    parser.add_argument('--occult-kml', nargs='+', default=None, type=str,
                        help='External OCCULT4 kml file (file name)')
    parser.add_argument('--central-line-occult-kml', default=None, type=str,
                        help='Central line reference OCCULT4 kml file (file name)')
    parser.add_argument('--observer-csv', nargs='+', default=None, type=str, help='observer csv file (file name)')
    parser.add_argument('--marker-style-csv', default=None, type=str,
                        help='observer marker style file (file name)')
    parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--debug', action='store_true', help='debug mode')
    parser.add_argument('--kml-fn', default=None, type=str, help='kml prediction file')
    parser.add_argument('--xml-namespace', default=None, type=str, help='XML namespace')
    parser.add_argument('--temp-20230309', action='store_true', help='Temporal correction for 20230309data')
    parser.add_argument('--no-sigma-lines', action='store_true', help='Exclude 1-sigma lines in prediction')
    parser.add_argument('--lw', '--line-width', default=5.0, type=float,
                        help='Zenith distance of the target [deg]')
    parser.add_argument('html_fn')
    args = parser.parse_args()

    print('# Version: {}'.format(__version__))
    # Debug.
    if args.debug:
        print('#[DEBUG] Current directory: {}'.format(os.getcwd()))
        print('#[DEBUG] Source path: {}'.format(os.path.dirname(os.path.abspath(__file__))))
        print('#[DEBUG] XML name space: {}'.format(FoliumMap.xml_namespace))
        print('#[DEBUG] arguments: {}'.format(args))

    # Set line width
    if args.lw:
        set_line_width(args.lw)

    # Change namespace of XML file.
    if args.xml_namespace:
        FoliumMap.set_xml_namespace(args.xml_namespace)

    # Boundary box of the map.
    bd = {'lng_min': args.range_lng[0], 'lng_max': args.range_lng[1],
          'lat_min': args.range_lat[0], 'lat_max': args.range_lat[1]}
    line_center = []  # Central line coordinates.
    fn_html_out = args.html_fn  # Output HTML file name.

    # KML file check (for Windows trouble).
    indx_fence: ndarray = np.array([])
    if args.range_fence_num:
        indx_fence = np.arange(args.range_fence_num[0], args.range_fence_num[1] + 1)
        if args.exclude_range_fence_num:
            indx_exclude_fence = np.arange(args.exclude_range_fence_num[0],
                                           args.exclude_range_fence_num[1] + 1)
            indx_fence = np.setdiff1d(indx_fence, indx_exclude_fence)
    elif args.range_fence:
        indx_fence = np.linspace(args.range_fence[0], args.range_fence[1],
                                 int((args.range_fence[1] - args.range_fence[0]) /
                                     args.interval_fence) + 1
                                 )

    # KML file reader. (maybe obsoleted in the future.)
    if args.kml_fn:
        fn_kml_in = args.kml_fn
        if os.path.isfile(fn_kml_in):
            print('Not found {}.'.format(fn_kml_in))
            # cwd = os.getcwd()
            # fn_kml_in_cwd = os.path.join(cwd, fn_kml_in)
            fn_kml_in_full = os.path.abspath(fn_kml_in)
            print('Search as {}'.format(fn_kml_in_full))
            if not os.path.isfile(fn_kml_in_full):
                sys.stderr.write('KML file not found. Ignored.\n'.format(fn_kml_in_full))
            else:
                fn_kml_in = fn_kml_in_full

    # Create FoliumMap instance (base map).
    fmap = FoliumMap(width=args.width, height=args.height, location=args.center, zoom_start=args.zoom)
    # Add tile layers to the base map. (Details are defined in the method.)
    fmap.add_tile_layers()

    """ Prediction layer. """
    # fmap.add_map_to_group(None, 'Prediction')

    # Append OCCULT4 prediction kml files.
    if args.occult_kml:
        j = 0
        for fn_kml in get_filenames_from_list_and_dir(args.occult_kml, ext='.km[lz]'):
            if args.debug:
                print(f'#[DEBUG] Occult kml file: {fn_kml}')
            fmap.make_group(f'Prediction{j:02d}')
            lines_predict, labels_predict, lines_obs = get_lines_from_occult_file(fn_kml, debug=args.debug)
            if args.debug:
                print('Found {} fences.'.format(len(lines_obs)))  # Number of fences in the file.
            line_center = lines_predict[0]  # Central line coordinates.

            # Special treatment for 2023/03/09 data. (begin)
            if args.temp_20230309:
                print('Temporal correction for 20230309 (insert +/-300m fences)')
                lines_obs = coords_correct_20230309_chugoku(lines_predict, lines_obs)
                print(len(lines_obs))

            fmap.make_group(f'OCCULT_Fences{j:02d}')
            lines_obs.insert(int(len(lines_obs) / 2), [labels_predict[0], lines_predict[0]])

            i = 0
            for vals in lines_obs:
                fence_no = int(len(lines_obs) / 2) - i
                if (i % args.tics_fence) == 0:
                    clr = '#ff3300'
                    weight = 2
                else:
                    clr = '#ff9933'
                    weight = 1
                fmap.add_polyline(vals[1],
                                  color=clr, weight=weight,
                                  tooltip='F{:+d} {}'.format(fence_no, vals[0]),
                                  group=f'OCCULT_Fences{j:02d}')
                i += 1

            # Remove empty list from lines_predict. (2023/10/05 HA)
            while True:
                try:
                    lines_predict.remove([])
                except ValueError:
                    break

            try:
                fmap.draw_prediction_lines(lines_predict, labels_predict,
                                           group=f'Prediction{j:02d}', no_sigma=args.no_sigma_lines)
            except ValueError:
                sys.stderr.write(f'# Prediction line drawing error ({fn_kml}).\n')
                # Debug messages.
                print(type(lines_predict))  # Debug.
                print(len(lines_predict))  # Debug.
                print(lines_predict)  # Debug.
            j += 1

    if args.central_line_occult_kml:
        _lines_wo_altcor, _, _ = get_lines_from_occult_file(args.central_line_occult_kml)
        line_center = _lines_wo_altcor[0]

    # Read geojson file.
    # fmap.read_geojson(fn_geojson_in)
    # Draw geojson items.
    if args.kml_fn is not None:
        fmap.make_group('Fences')
        # fmap.add_map_to_group(None, 'Fences')

        fn_kml_in = args.kml_fn
        lines_predict = get_prediction_lines_from_occult_kml(fn_kml_in)

        # Cut out lines in the boundary.
        lines_prdct_cut = [cut_line_in_area(line, bd['lng_min'], bd['lng_max'], bd['lat_min'], bd['lat_max'])
                           for line in lines_predict]

        # Draw observation lines (fences).
        # fmap.draw_geojson_linestring(group='Prediction')
        if args.debug:
            print(indx_fence)
        fmap.draw_fences(lines_prdct_cut[0], indx_fence, args.interval_fence,
                         group='Fences', interval=args.tics_fence,
                         factor=args.factor_fence_interval)  # lat. diff. arrays.

        # Draw prediction lines.
        fmap.draw_prediction_lines(lines_prdct_cut, labels_predict, group='Prediction')
        # fmap.draw_geojson_point(group='Observers')
    elif args.with_fences:
        fmap.make_group('Fences')
        # fmap.add_map_to_group(None, 'Fences')
        if args.debug:
            print(indx_fence)

        fmap.draw_fences(cut_line_in_area(line_center,
                                          bd['lng_min'], bd['lng_max'], bd['lat_min'], bd['lat_max']),
                         indx_fence, args.interval_fence,
                         group='Fences', interval=args.tics_fence,
                         factor=args.factor_fence_interval)  # lat. diff. arrays.

    # Marker style configuration.
    if args.marker_style_csv:
        define_observer_marker_style(args.marker_style_csv)

    # Read observer information from the CSV files.
    if args.observer_csv:
        i = 0
        for fn_csv in get_filenames_from_list_and_dir(args.observer_csv, ext='.csv'):
            grpname = f'Observers{i:02d}'
            fmap.make_group(grpname)
            fmap.add_observers_markers(read_observers_csv(fn_csv), grpname)
            i += 1

    # Finalize map.
    fmap.add_groups_to_map()  # Add groups.
    fmap.add_layer_control()  # Layer control.
    fmap.add_fullscreen()  # Full screen button.
    fmap.add_measure_control()  # Measure control plugin.
    fmap.add_minimap()  # Mini map plugin.
    fmap.add_locate_control()  # Locate control button.

    # File output.
    fmap.save_html(fn_html_out)
    # map.add_json('https://1601-031.a.hiroshima-u.ac.jp/~akitaya/gsimap_test/formatter.geojson')
    webbrowser.open(fn_html_out)
