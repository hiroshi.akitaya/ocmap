#!/usr/bin/env python3
#
#   Folium map for occultation observation.
#
# Hirsohi Akitaya
#   Ver 0.20: 2022-12-11; Add options. Line color/width modified.
#   Ver 0.21: 2022-12-13; Options added. (boundary, line ranges, etc.)
#   Ver 0.22: 2022-12-16; Add LocateControl, read topological kml file.
#   Ver 0.23: 2022-12-30; Read observers. File name changed (gsimap_test.py -> ocmap.py)

__version__ = '0.23'
__author__ = 'Hiroshi Akitaya'

import json
import os
import sys
import csv
from typing import List

import folium
import numpy as np
import requests
from folium import FeatureGroup, LayerControl
from folium import plugins
from folium.features import DivIcon
from folium.features import LatLngPopup
from jinja2 import Template

# LOC_CENTER = [34.649441, 135.001304]  # Center coordinate.
_LOC_CENTER_DEFAULT = [34.043557, 132.846680]  # CC2021-2023-02-06
_WIDTH_DEFAULT = 800
_HEIGHT_DEFAULT = 600
_ZOOM_DEFAULT = 8

FUNCTIONS_JS = './functions.js'
URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png'
# URL_BASE = 'OpenStreetMap'
URL_MAP1 = 'https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg'
ATTR_DEFAULT = 'GSI'



class FoliumMap(object):

    observer_plot_style = {
        '1': {'color': '#ff0000', 'radius': 10,},
        '2': {'color': '#0000ff', 'radius': 10,},
        '3': {'color': '#ff0000', 'radius': 6,},
        '4': {'color': '#0000ff', 'radius': 6,},
    }

    def __init__(self, location=_LOC_CENTER_DEFAULT,
                 width=_WIDTH_DEFAULT, height=_HEIGHT_DEFAULT,
                 center=_LOC_CENTER_DEFAULT,
                 zoom_start=_ZOOM_DEFAULT):
        self.gj = None
        self.m = folium.Map(location=location,
                            width=width, height=height,
                            tiles=URL_BASE,
                            name=u'国土地理院 標準地図',
                            attr=u'<a href="https://www.gsi.go.jp/">国土地理院</a>',
                            control_scale=True,  # Show scale.
                            zoom_start=zoom_start)
        # Read Javascript file.
        self.m.get_root().html.add_child(folium.JavascriptLink(FUNCTIONS_JS))
        self.groups = {}
        # self.m.add_child(folium.LatLngPopup())
        self.m.add_child(GetLatLngPopup())
        self.jg = None  # Geojson values.

    @staticmethod
    def define_observer_plot_style(style_csv_fn, debug=False):
        with open(style_csv_fn, 'r', encoding='utf-8-sig') as f:
            reader = csv.reader(f)
            for row in reader:
                print(row)
                try:
                    FoliumMap.observer_plot_style[row[0]] = {'color': row[1],
                                                             'radius': float(row[2])
                                                             }
                except ValueError:
                    sys.stderr.write('Wrong plot style.\n')
        if debug:
            print(FoliumMap.observer_plot_style)


    def add_tile_layers(self):
        #        folium.TileLayer(
        #            tiles='https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png',
        #            name='GSI standard map',
        #            attr='GSI'
        #        ).add_to(self.m)
        folium.TileLayer(
            tiles='https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg',
            name=u'国土地理院 最新航空写真',
            attr=u'Data by <a href="https://www.gsi.go.jp/">国土地理院</a>',
        ).add_to(self.m)
        folium.TileLayer(
            # https://maps.gsi.go.jp/development/ichiran.html#relief
            tiles='https://cyberjapandata.gsi.go.jp/xyz/relief/{z}/{x}/{y}.png',
            name=u'国土地理院 色別標高図',
            attr='<a href="https://www.gsi.go.jp/">国土地理院</a>  (海域部は海上保安庁海洋情報部の資料を使用して作成)',
        ).add_to(self.m)
        folium.TileLayer(
            tiles='openstreetmap',
        ).add_to(self.m)

    def add_locate_control(self):
        plugins.LocateControl().add_to((self.m))

    def add_layer_control(self):
        LayerControl().add_to(self.m)  # Layer control.

    def add_fullscreen(self):
        plugins.Fullscreen(
            position="topright",  # bottomleft
            title="Fullscreen",
            title_cancel="Back",
            # force_separate_button=True,
        ).add_to(self.m)

    def make_group(self, name_group):
        self.groups[name_group] = FeatureGroup(name=name_group)

    def add_groups_to_map(self):
        for group in self.groups.values():
            # print(group)
            group.add_to(self.m)

    def add_map_to_group(self, url, group):
        folium.Map(tiles=url, attr='GSI').add_to(self.groups[group])

    def _get_map_instance(self, group):
        if (group is None) or (group not in self.groups):
            return self.m
        else:
            return self.groups[group]

    def add_observers_markers(self, observers: list, group: str = None):
        for observer in observers:
            name = observer[0]
            try:
                coord = [float(observer[2]), float(observer[1])]  # Lat, Lng
            except ValueError:
                sys.stderr.write('Wrong Lng/Lat format.\n')
            observer_type = observer[3]
            if not observer_type in FoliumMap.observer_plot_style:
                sys.stderr.write('Observer type {} not defined.'.format(observer_type))
            color = FoliumMap.observer_plot_style[observer_type]['color']
            radius = FoliumMap.observer_plot_style[observer_type]['radius']
            self.add_marker(coord, popup_name=name, fill=True, color=color,
                            tooltip=name, radius=radius, group=group)


    def add_marker(self, coord: list, popup_name: str, fill: bool,
                   color: str, tooltip: str = None,
                   radius: float = 10.0, group: str = None):
        """
        :list coord:
        :str popup_name:
        :bool fill: Fill marker (True or False).
        :str color:
        :str tooltip:
        :float radius:
        :bool group:
        """
        coord_str = '<a href="http://www.google.com/maps/place/{0:},{1:}" target="_blank" title="Google Map">{0:.6f},' \
                    '{1:.6f}</a>'.format(coord[0], coord[1])
        elevation_str = 'Elevation: <a href="{}" target="_blank">GSI API</a>'.format(get_gsi_elevation_api_url(coord))
        # Supress vertical style for Japanese letters.
        # https://qiita.com/yuta_muds/items/1a4762ae0a9aaa4c783d
        popup = ''
        popup += '<span style="white-space: nowrap;">' + \
                 'Registerd Point</br>' + \
                 popup_name + '</br>' + coord_str + '</br>' + elevation_str + \
                 '<span id="elevation"></span>'
        # popup += '<script>get_gsi_elevation({}, {});</script>'.format(coord[0], coord[1])
        folium.CircleMarker(coord, radius=radius, popup=popup, fill=fill,
                            color=color, tooltip=tooltip, fill_color=color).add_to(
            self._get_map_instance(group)
        )
        # print('Adding marker: {} {} {} {} to group: {}'.format(coord, popup, fill, color, group))

    def add_string_marker(self, coord, text, color='#000000', group=None):
        folium.Marker(
            coord,
            icon=DivIcon(
                icon_size=(250, 36),
                icon_anchor=(0, 0),
                # html='<div style="font-size: 10pt" text_color="{}">{}</div>'.format(color, text),
                html='<div style="font-size: 10pt; color: {}" alt="{}">{}</div>'.format(color, text, text)
                # html = '<div>{}</div>'.format(text)
            )
        ).add_to(self._get_map_instance(group))
        # print('<div style="font-size: 10pt">{}</div>'.format(text))

    def add_markers(self, mymarkers):
        for mk in mymarkers:
            self.add_marker(mk.coord, mk.observer, mk.fill, mk.color)

    def add_polyline(self, coords_in, color='#0000ff', tooltip=None, wcwidth=1,
                     weight=1, group=None):
        folium.PolyLine(locations=coords_in, tooltip=tooltip, wcwidth=wcwidth,
                        weight=weight,
                        color=color).add_to(self._get_map_instance(group))
        # print('Adding polyline'.format())

    def save_html(self, fn: str):
        """ Save html file with Leaflet JavaScript codes.
        :str fn: Output file name (html).
        """
        try:
            self.m.save(fn)
        except OSError:
            sys.stderr.write('File output failed: {}'.format(fn))
            sys.exit(1)

    def add_json(self, fn_json: str):
        # print(fn_json)
        folium.TopoJson(
            json.loads(requests.get(fn_json).text),
            "objects.antarctic_ice_shelf",
            name="topojson",
        ).add_to(self.m)

        folium.LayerControl().add_to(self.m)

        # folium.GeoJson(fn_json, name="geojson").add_to(self.m)

    def read_geojson(self, fn):
        status = 1
        with open(fn, 'r') as f:
            self.gj = json.load(f)
            status = 0
        return status

    def get_geojson_linecoords(self):
        i = 0
        for item in self.gj['features']:
            i += 1
            if item['geometry']['type'] == 'LineString':
                if i < 4:
                    continue
                coordinates = item['geometry']['coordinates']
                return coordinates_reverse(coordinates)

    def draw_geojson_seplines(self, seps, coords_in):
        for sep in seps:
            coords_sep = calc_separated_coords(sep, coords_in)
            # print(coords_sep)
            self.add_polyline(coords_sep)

    def draw_obslines_tmp(self, coords_in, seps, group, interval=5):
        """ Test code to draw obs lines.
        :list coords_in:
        :list seps:
        :str group:
        """
        l0_index = _calc_l0_index(seps)
        i = 0
        for sep in seps:
            n_sep = int(i - l0_index)
            # clr = '#ff{:04x}'.format(int(65535 * i / ncol))
            clr = '#ff9933'  # orange (safe color) for default.
            if (i % interval) == 0:
                clr = '#ff3300'  # Dark orange for every (interval) line.
            i += 1
            coord_new = calc_separated_coords(sep, coords_in)
            # print(coord_new[0])
            # Line.
            self.add_polyline(coord_new, color=clr, tooltip='L{:+d} ({:.2f} km)'.format(n_sep, sep),
                              group=group)
            # Obs. line label.
            for j in np.linspace(0, len(coord_new) - 1, interval):
                j = int(j)
                # print(j, coord_new[j])
                self.add_string_marker(coord_new[j], 'L{:+d}'.format(n_sep),
                                       color=clr, group=group)

    def draw_geojson_linestring(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'LineString':
                coordinates = item['geometry']['coordinates']
                self.add_polyline(coordinates_reverse(coordinates), group=group)

    def draw_prediction_lines(self, lines: list, group=None, color_def=None):
        if color_def is None:
            color_def = {'center': 'red', 'boundary': 'blue', 'sigma1': 'green'}
        if len(lines) < 5:
            return 1
        self.add_polyline(lines[0], color=color_def['center'], tooltip='Prediction Center',
                          wcwidth=3, weight=3, group=group)
        self.add_polyline(lines[1], weight=3, color=color_def['boundary'], tooltip='Prediction Boundary (+)',
                          wcwidth=3, group=group)
        self.add_polyline(lines[2], color=color_def['boundary'], tooltip='Prediction Boundary (-)',
                          wcwidth=3, weight=3, group=group)
        self.add_polyline(lines[3], weight=3, color=color_def['sigma1'], tooltip='Prediction 1-sigma (+)',
                          wcwidth=3, group=group)
        self.add_polyline(lines[4], color=color_def['sigma1'], tooltip='Prediction 1-sigma (-)',
                          wcwidth=3, weight=3, group=group)

    def draw_geojson_point(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'Point':
                coordinate = item['geometry']['coordinates']
                coordinate.reverse()
                self.add_marker(coordinate, item['properties']['name'],
                                True, '#ff0000', tooltip=item['properties']['name'],
                                group=group)

    def draw_geojson_diagrams(self, group=None):
        for item in self.gj['features']:
            if item['geometry']['type'] == 'LineString':
                coordinates = item['geometry']['coordinates']
                self.add_polyline(coordinates_reverse(coordinates), group=group)
            elif item['geometry']['type'] == 'Point':
                coordinate = item['geometry']['coordinates']
                coordinate.reverse()
                self.add_marker(coordinate, item['properties']['name'],
                                True, '#ff0000', group=group)

    def draw(self):
        folium.LayerControl().add_to(self.m)


class MyMarker(object):
    def __init__(self, coord, observer, fill=True, color=''):
        self.coord = coord
        self.observer = observer
        self.fill = fill
        self.color = color


def read_observers_csv(fn_in) -> list:
    """ Read observer csv file.
    """
    observers = []
    with open(fn_in, 'r', encoding='utf-8-sig') as f:
        reader = csv.reader(f)
        observers = [row for row in reader]
        observers.pop(0)
    return observers

def get_topographic_corrected_occult_kml(fn: str, verbose=False) -> list:
    """
    Get Topographic Corrected fences from OCCULT kml file.
    :param fn: input file name.
    :param verbose: flag verose mode.
    :return: Coordinates list.
    """
    import xml.etree.ElementTree
    n_lines_found = 0
    lines = []
    tree = xml.etree.ElementTree.parse(fn)
    root = tree.getroot()

    for child in root[0]:
        if child.tag.endswith('Placemark'):
            coords = child[2][2].text.split()
            lines.append(np.array(split_xyz_coords(coords)))
            n_lines_found += 1
    if verbose:
        print('Number of lines found: {}'.format(n_lines_found))
    return lines


def get_prediction_lines_from_occult_kml(fn: str, verbose=False) -> list:
    """
    Get prediction lines (5 lines) form Occult prediction kml file.
    """
    import xml.etree.ElementTree
    n_lines_found = 0
    lines = []
    tree = xml.etree.ElementTree.parse(fn)
    root = tree.getroot()
    for linestring in root.iter():
        if linestring.tag != 'coordinates':
            continue
        coords = linestring.text.split()
        lines.append(np.array(split_xyz_coords(coords)))
        n_lines_found += 1
    if verbose:
        print('Number of lines found: {}'.format(n_lines_found))
    return lines


def split_xyz_coords(coords: list) -> list:
    """
    Convert (lat, lng, height) coordinates strings
    into (lng, lat) coordinates.
    :list coords: Coordinates; list of (Longitude, Latitude).
    :list: List of (Longitude, Latitude) coordinates.
    """
    coords_xy: list[list[float]] = []  # Initialize coordinate list.
    for coord in coords:
        if len(coord) < 2:
            raise ValueError
        vals = coord.split(',')
        try:
            coords_xy.append([float(vals[1]),
                              float(vals[0])])  # Append [lng, lat].
        except ValueError:
            pass
    return coords_xy


def _calc_l0_index(seps: list) -> int:
    """
    Calculate central index with zero value from the discrete list at even interval.
    :seps: Values list.
    :return: Zero-value index.
    """
    return int((-1.0) * seps[0] * (len(seps) - 1) / (seps[-1] - seps[0]))


def cut_line_in_area(line: np.ndarray, lng_min: float, lng_max: float,
                     lat_min: float, lat_max: float) -> np.ndarray:
    """ Cut out line coordinates (2-d numpy.ndarray) within
    [lng_min:lng_max, lat_min:lat_max] coordinate window.
    """
    lt = line.T
    indx = np.intersect1d(np.where((lt[0] > lng_min) & (lt[0] < lng_max)),
                          np.where((lt[1] > lat_min) & (lt[1] < lat_max))
                          )
    newline = line[indx]
    return newline


def calc_separated_latlng(sep: float,
                          lat_deg: float, lng_deg: float,
                          d_lat_deg: float, d_lng_deg: float) -> (float, float):
    """ Calculate a new lat-lng coordinate with a distance of sep km.
    """
    R_EARTH = 6371.0  # km
    psi = np.arctan2(d_lat_deg, d_lng_deg * np.cos(lat_deg / 180 * np.pi))  # PA of the reference vector.
    psi_perp = psi - np.pi / 2  # PA perpendicular to the ref. vector.
    sep_lat_deg = sep / R_EARTH * np.sin(psi_perp) * 180 / np.pi  # New latitude.
    sep_lng_deg = sep / R_EARTH * np.cos(psi_perp) / np.cos(lat_deg / 180 * np.pi) * 180 / np.pi  # New longitude.
    return lat_deg + sep_lat_deg, lng_deg + sep_lng_deg


def calc_separated_coords(sep: float, coords_in: list) -> list:
    # print(sep)
    coords_sep = []
    coord_pre = None
    for coord in coords_in:
        if coord_pre is None:
            coord_pre = coord
            continue
        lat, lng = coord
        lat_pre, lng_pre = coord_pre
        coords_sep.append(calc_separated_latlng(sep, lat, lng, lat - lat_pre, lng - lng_pre))
        coord_pre = coord
    return coords_sep


def coordinates_reverse(coords_in):
    coords_rev = []
    for coord in coords_in:
        coords_rev.append([coord[1], coord[0]])
    return coords_rev


def get_gsi_elevation_api_url(coord):
    # Ref. https://www.gis-py.com/entry/elevation-api
    # https://maps.gsi.go.jp/development/elevation_s.html
    lon = coord[1]
    lat = coord[0]
    url = 'http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon={}&lat={}'.format(lon, lat)
    return url


# Redefine LatLngPopup with higher decimal precision.
#  Ref. https://gis.stackexchange.com/questions/371628/get-coordinates-from-foliums-feature-latlngpopup-in-python
#
class GetLatLngPopup(LatLngPopup):
    _template = Template(u"""
            {% macro script(this, kwargs) %}
                var {{this.get_name()}} = L.popup();
                function latLngPop(e) {
                    {{this.get_name()}}
                        .setLatLng(e.latlng)
                        .setContent("<a href='http://www.google.com/maps/place/" +
                                e.latlng.lat.toFixed(6) + "," + e.latlng.lng.toFixed(6) + 
                                "' target='_blank' title='Google Map'>" + 
                                e.latlng.lat.toFixed(6) + "," + e.latlng.lng.toFixed(6) +
                                "</a><br />" +
                                "<a href='http://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon=" +
                                e.latlng.lng.toFixed(6) +"&lat=" + e.latlng.lat.toFixed(6) + 
                                "' target='_blank' title='GSI API JSON file'>El.: </a>" +
                                "<span id='elevation'></span> m"
                                )
                        .openOn({{this._parent.get_name()}});
                        get_gsi_elevation(e.latlng.lat, e.latlng.lng);
                    }
                {{this._parent.get_name()}}.on('click', latLngPop);
            {% endmacro %}
            """)

    def __init__(self):
        super(GetLatLngPopup, self).__init__()
        self._name = 'GetLatLngPopup'


if __name__ == '__main__':
    _LOC_CENTER_DEFAULT = [34.043557, 132.846680]
    _RANGE_LNG_DEFAULT = [20.0, 46.0]
    _RANGE_LAT_DEFAULT = [122.0, 154.0]
    _RANGE_FENCE_DEFAULT = [-50, 50]
    _INTERVAL_FENCE_DEFAULT = 5
    _TICS_FENCE_DEFAULT = 5
    import argparse
    import webbrowser

    # Option analysis.
    parser = argparse.ArgumentParser()
    parser.add_argument('--width', default=800, type=int,
                        help='map width [pixels]')
    parser.add_argument('--height', default=600, type=int,
                        help='map height [pixels]')
    parser.add_argument('--zoom', default=8, type=int,
                        help='initial zoom level [integer]')
    parser.add_argument('--center', nargs=2, default=_LOC_CENTER_DEFAULT, type=float,
                        help='center coordinate (lng[deg lat[deg])')
    parser.add_argument('--range-lng', nargs=2, default=_RANGE_LNG_DEFAULT, type=float,
                        help='longitude range (min[deg] max[deg])')
    parser.add_argument('--range-lat', nargs=2, default=_RANGE_LAT_DEFAULT, type=float,
                        help='latitude range (min[deg] max[deg])')
    parser.add_argument('--range-fence', '--range-lines', nargs=2,
                        default=_RANGE_FENCE_DEFAULT, type=float,
                        help='range of fence (min[km] max[km])')
    parser.add_argument('--interval-fence', '--interval-lines',
                        default=_INTERVAL_FENCE_DEFAULT, type=float,
                        help='interval of fences [km]')
    parser.add_argument('--tics-fence', '--tics-lines',
                        default=_TICS_FENCE_DEFAULT, type=int,
                        help='color emphasis intrval of fences [integer]')
    parser.add_argument('--add-kml',
                        default=None, type=str,
                        help='external kml file (file name)')
    parser.add_argument('--observer-csv',
                        default=None, type=str,
                        help='observer csv file (file name)')
    parser.add_argument('--marker-style-csv',
                        default=None, type=str,
                        help='observer marker style file (file name)')
    parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__))

    parser.add_argument('--debug', action='store_true', help='debug mode')
    parser.add_argument('kml_fn')
    parser.add_argument('html_fn')
    args = parser.parse_args()
    print(args)

    # Debug.
    if args.debug:
        print('#[DEBUG] Current directory: {}'.format(os.getcwd()))
        print('#[DEBUG] Source path: {}'.format(os.path.dirname(os.path.abspath(__file__))))

    boundary_lnglat = {'lng_min': args.range_lng[0], 'lng_max': args.range_lng[1],
                       'lat_min': args.range_lat[0], 'lat_max': args.range_lat[1]}  # boundary.

    fn_kml_in = args.kml_fn
    fn_html_out = args.html_fn

    # KML file check (for Windows trouble).

    if args.debug:
        print('#[Debug] KML file full path: {}'.format(os.path.abspath(fn_kml_in)))

    if not os.path.isfile(fn_kml_in):
        print('Not found {}.'.format(fn_kml_in))
        #cwd = os.getcwd()
        #fn_kml_in_cwd = os.path.join(cwd, fn_kml_in)
        fn_kml_in_full = os.path.abspath(fn_kml_in)
        print('Search as {}'.format(fn_kml_in_full))
        if not os.path.isfile(fn_kml_in_full):
            sys.stderr.write('KML file not found. Abort.\n'.format(fn_kml_in_full))
            sys.exit(1)
        else:
            fn_kml_in = fn_kml_in_full

    fmap = FoliumMap(width=args.width, height=args.height, location=args.center,
                     zoom_start=args.zoom)

    fmap.add_tile_layers()  # Add tile layers. (Details are defined in the method.)

    if args.add_kml is not None:
        try:
            lines_add_kml = get_topographic_corrected_occult_kml(args.add_kml)
        except Exception:
            print('File {} not found. Skip.'.format(args.add_kml))
        else:
            fmap.make_group('Additional_Lines')
            fmap.add_map_to_group(None, 'Additional_Lines')
            for line in lines_add_kml:
                fmap.add_polyline(line, color='#663333', weight=1, group='Additional_Lines')


    fmap.make_group('Lines')
    fmap.add_map_to_group(None, 'Lines')
    fmap.make_group('Prediction')
    fmap.add_map_to_group(None, 'Prediction')

    fmap.make_group('Observers')
    fmap.add_map_to_group(None, 'Observers')

    if args.marker_style_csv is not None:
        FoliumMap.define_observer_plot_style(args.marker_style_csv)

    if args.observer_csv is not None:
        fmap.add_observers_markers(read_observers_csv(args.observer_csv), 'Observers')

    # Read geojson file.
    # fmap.read_geojson(fn_geojson_in)
    # Draw geojson items.
    lines_prdct = get_prediction_lines_from_occult_kml(fn_kml_in)
    lines_prdct_cut = []

    # Cut out lines in the boundary.
    for line in lines_prdct:
        lines_prdct_cut.append(cut_line_in_area(
            line,
            boundary_lnglat['lng_min'], boundary_lnglat['lng_max'],
            boundary_lnglat['lat_min'], boundary_lnglat['lat_max']))

    # Draw observation lines (fences).
    # fmap.draw_geojson_linestring(group='Prediction')
    fmap.draw_obslines_tmp(lines_prdct_cut[0],
                           np.linspace(args.range_fence[0], args.range_fence[1],
                                       int((args.range_fence[1] - args.range_fence[0]) / args.interval_fence) + 1
                                       ),
                           group='Lines', interval=args.tics_fence)  # lat. diff. arrays.

    # Draw prediction lines.
    fmap.draw_prediction_lines(lines_prdct_cut, group='Prediction')
    # fmap.draw_geojson_point(group='Observers')

    # Test to draw observational lines.
    # coords = fmap.get_geojson_linecoords()

    # Plugins.
    # Finalize map.
    fmap.add_groups_to_map()
    fmap.add_layer_control()
    fmap.add_fullscreen()
    fmap.add_locate_control()

    # Output file.
    fmap.save_html(fn_html_out)
    # map.add_json('https://1601-031.a.hiroshima-u.ac.jp/~akitaya/gsimap_test/formatter.geojson')
    webbrowser.open(fn_html_out)
