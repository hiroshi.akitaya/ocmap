#!/usr/bin/env python3

import sys
import json
import requests

import numpy as np

import folium
from folium.features import DivIcon

LOC_CENTER = [34.8, 134.72]  # Center coordinate.
ZOOM_DEFAULT = 10

URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png'
#URL_BASE = 'OpenStreetMap'
#URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg'
ATTR_DEFAULT = 'GSI'

class GsiMap(object):

    def __init__(self):
        self.m = folium.Map(location=LOC_CENTER,
                            tiles=URL_BASE,
                            attr='GSI',
                            zoom_start=ZOOM_DEFAULT)
        self.m.add_child(folium.LatLngPopup())
        self.jg = None  # Geojson values.
        
    def add_marker(self, coord, popup, fill, color):
        # Supress vertical style for Japanese letters.
        # https://qiita.com/yuta_muds/items/1a4762ae0a9aaa4c783d
        popup = '<span style="white-space: nowrap;">' + popup + '</span>'
        folium.CircleMarker(coord, radius=5, popup=popup, fill=fill,
                            color=color, fill_color=color).add_to(self.m)
        print('Adding marker: {} {} {} {}'.format(coord, popup, fill, color))

    def add_string_marker(self, coord, text, color='#000000'):
        folium.Marker(
            coord,
            icon=DivIcon(
                icon_size=(250, 36),
                icon_anchor=(0, 0),
                #html='<div style="font-size: 10pt" text_color="{}">{}</div>'.format(color, text),
                html='<div style="font-size: 10pt; color: {}" alt="{}">{}</div>'.format(color, text, text)
                #html = '<div>{}</div>'.format(text)
            )
        ).add_to(self.m)
        print('<div style="font-size: 10pt">{}</div>'.format(text))

    def add_markers(self, mymarkers):
        for mk in mymarkers:
            self.add_marker(mk.coord, mk.observer, mk.fill, mk.color)

    def add_polyline(self, coords, color='#0000ff'):
        folium.PolyLine(locations=coords, color=color).add_to(self.m)
        print('Adding polyline'.format())

    def save_html(self, fn):
        try:
            self.m.save(fn)
        except Exception:
            sys.stderr.write('File output failed: {}'.format(fn))
            sys.exit(1)

    def add_json(self, fn_json):
        print(fn_json)
        folium.TopoJson(
            json.loads(requests.get(fn_json).text),
            "objects.antarctic_ice_shelf",
            name="topojson",
        ).add_to(self.m)

        folium.LayerControl().add_to(self.m)

        #folium.GeoJson(fn_json, name="geojson").add_to(self.m)

    def read_geojson(self, fn):
        status = 1
        with open(fn, 'r') as f:
            self.gj = json.load(f)
            status = 0
        return status

    def get_geojson_linecoords(self):
        i=0
        for item in self.gj['features']:
            i += 1
            if item['geometry']['type'] == 'LineString':
                if i < 4:
                    continue
                coordinates = item['geometry']['coordinates']
                return coordinates_reverse(coordinates)

    def draw_geojson_seplines(self, seps, coords):
        for sep in seps:
            coords_sep = calc_separated_coords(sep, coords)
            print(coords_sep)
            self.add_polyline(coords_sep)

    def draw_obslines_tmp(self, coords, seps):
        """ Test code to draw obs lines.
        """
        ncol = len(seps)
        i =0
        for sep in seps:
            clr = '#ff{:04x}'.format(int(65535*i/ncol))
            i += 1
            coord_new = []
            for coord in coords:
                coord_new.append([coord[0] + sep, coord[1]])
            # Line.
            self.add_polyline(coord_new, color=clr)
            #Text.
            for j in np.linspace(0, len(coord_new)-1, 60):
                j = int(j)
                print(j, coord_new[j])
                self.add_string_marker(coord_new[j], 'L{}'.format(i), color=clr)

    def draw_geojson_diagrams(self):
        for item in self.gj['features']:
            if item ['geometry']['type'] == 'LineString':
                coordinates = item['geometry']['coordinates']
                self.add_polyline(coordinates_reverse(coordinates))
            elif item['geometry']['type'] == 'Point':
                coordinate = item['geometry']['coordinates']
                coordinate.reverse()
                self.add_marker(coordinate, item['properties']['name'],
                                True, '#ff0000')

    def draw(self):
        folium.LayerControl().add_to(self.m)

class MyMarker(object):
    def __init__(self, coord, observer, fill=True, color=''):
        self.coord = coord
        self.observer = observer
        self.fill = fill
        self.color = color

R_EARTH = 6371.0  # km
def calc_separated_lnglat(sep, lat, lng, dlat, dlng, r_earth=R_EARTH):
    psi = np.arctan2(lat, dlng * np.cos(lat/np.pi*180.0))
    sep_lat = sep/r_earth*np.cos(psi)/np.pi*180.0
    return lat+sep_lat, lng

def calc_separated_coords(sep, coords):
    coords_sep = []
    coord_pre = None
    for coord in coords:
        if coord_pre is None:
            coord_pre = coord
            continue
        lat, lng = coord
        lat_pre, lng_pre = coord
        coords_sep.append(calc_separated_lnglat(sep, lat, lng, lat-lat_pre, lng-lng_pre))
        coord_pre = coord
    return coords_sep

def coordinates_reverse(coords):
    coords_rev = []
    for coord in coords:
        coords_rev.append([coord[1], coord[0]])
    return coords_rev


def read_phaethon_report_csv():
    import csv
    fn_in = '20221021Phaethon_Report.csv'
    tbl = []
    with open(fn_in) as f:
        reader = csv.reader(f)
        tbl = [row for row in reader]
    mymarkers= []
    for (observer, str_lon, str_lat, status) in zip(tbl[4], tbl[9], tbl[10], 
                                                    tbl[17]):
        if observer == '':
            continue
        lat = coord_str_resolve(str_lat)
        lon = coord_str_resolve(str_lon)
        
        if lat != None and lon != None:
            stat = status_resolve(status)
            print(status)
            if stat == 'observed':
                fill = True
                color = '#ff0000'
            elif stat == 'pass':
                fill = False
                color = '#00ff00'
            else:
                fill = False
                color = '#0000ff'
            mk = MyMarker([lat, lon], observer, fill, color)
            mymarkers.append(mk)
    return mymarkers

def status_resolve(status):
    if status[0] == 'Y':
        return 'observed'
    elif status[0] == 'N':
        return 'pass'
    else:
        return 'failed'

def coord_str_resolve(coord_str):
    import re
    from astropy.coordinates import Angle
    import astropy.units as u

    pattern = '(\d+)\D+(\d+)\D+(\d+)*.'
    repattern = re.compile(pattern)
    result = repattern.match(coord_str)
    if result == None:
        coord = None
    else:
        coord_str = '{}d{}m{}s'.format(result.group(1), 
                                       result.group(2), result.group(3))
        # print(coord_str)  # Debug.
        try:
            coord = Angle(coord_str).to(u.deg).value
        except Exception:
            coord = None
    return coord


if __name__ == '__main__':
    import webbrowser
    if len(sys.argv) <3:
        print('Usage: {} geojson_fn output_html_fn'.format(sys.argv[0]))
        sys.exit(1)
    fn_geojson_in = sys.argv[1]
    fn_html_out = sys.argv[2]

    map = GsiMap()
    # Read geojson file.
    map.read_geojson(fn_geojson_in)
    # Draw geojson items.
    map.draw_geojson_diagrams()

    # Test to draw observational lines.
    coords = map.get_geojson_linecoords()
    map.draw_obslines_tmp(coords, np.linspace(-0.4,0.4,9))  # lat. diff. arrays.

    # Finalize map.
    map.draw()

    # Output file.
    map.save_html(fn_html_out)
    # map.add_json('https://1601-031.a.hiroshima-u.ac.jp/~akitaya/gsimap_test/formatter.geojson')
    webbrowser.open(fn_html_out)