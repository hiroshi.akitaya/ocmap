from oldver.gsimap_test import MyMarker


def read_phaethon_report_csv():
    import csv
    fn_in = '20221021Phaethon_Report.csv'
    tbl = []
    with open(fn_in) as f:
        reader = csv.reader(f)
        tbl = [row for row in reader]
    mymarkers = []
    for (observer, str_lon, str_lat, status) in zip(tbl[4], tbl[9], tbl[10],
                                                    tbl[17]):
        if observer == '':
            continue
        lat = coord_str_resolve(str_lat)
        lon = coord_str_resolve(str_lon)

        if lat != None and lon != None:
            stat = status_resolve(status)
            print(status)
            if stat == 'observed':
                fill = True
                color = '#ff0000'
            elif stat == 'pass':
                fill = False
                color = '#00ff00'
            else:
                fill = False
                color = '#0000ff'
            mk = MyMarker([lat, lon], observer, fill, color)
            mymarkers.append(mk)
    return mymarkers


def coord_str_resolve(coord_str):
    import re
    from astropy.coordinates import Angle
    import astropy.units as u

    pattern = '(\d+)\D+(\d+)\D+(\d+)*.'
    repattern = re.compile(pattern)
    result = repattern.match(coord_str)
    if result == None:
        coord = None
    else:
        coord_str = '{}d{}m{}s'.format(result.group(1),
                                       result.group(2), result.group(3))
        # print(coord_str)  # Debug.
        try:
            coord = Angle(coord_str).to(u.deg).value
        except Exception:
            coord = None
    return coord

def status_resolve(status):
    if status[0] == 'Y':
        return 'observed'
    elif status[0] == 'N':
        return 'pass'
    else:
        return 'failed'
