#!/usr/bin/env python3

import sys
import json
import requests

import folium

LOC_CENTER = [43.19639, 141.44012]  # Center coordinate.
ZOOM_DEFAULT = 10

#URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png'
#URL_BASE = 'OpenStreetMap'
URL_BASE = 'https://cyberjapandata.gsi.go.jp/xyz/seamlessphoto/{z}/{x}/{y}.jpg'
#FN_OUT = 'test01.html'
FN_OUT = 'test03.html'
ATTR_DEFAULT = 'GSI'

class GsiMap(object):

    def __init__(self):
        self.m = folium.Map(location=LOC_CENTER,
                            tiles=URL_BASE,
                            attr='GSI',
                            zoom_start=ZOOM_DEFAULT)
        self.m.add_child(folium.LatLngPopup())
        
    def add_marker(self, coord, popup, fill, color):
        folium.CircleMarker(coord, radius=5, popup=popup, fill=fill,
                            color=color, fill_color=color).add_to(self.m)
        print('Adding marker: {} {} {} {}'.format(coord, popup, fill, color))

    def add_markers(self, mymarkers):
        for mk in mymarkers:
            self.add_marker(mk.coord, mk.observer, mk.fill, mk.color)
        
    def save_html(self, fn):
        try:
            self.m.save(fn)
        except Exception:
            sys.stderr.write('File output failed: {}'.format(fn))
            sys.exit(1)

    def add_json(self, fn_json):
        print(fn_json)
        folium.TopoJson(
            json.loads(requests.get(fn_json).text),
            "objects.antarctic_ice_shelf",
            name="topojson",
        ).add_to(self.m)

        folium.LayerControl().add_to(self.m)

        #folium.GeoJson(fn_json, name="geojson").add_to(self.m)

    def draw(self):
        folium.LayerControl().add_to(self.m)

class MyMarker(object):
    def __init__(self, coord, observer, fill=True, color=''):
        self.coord = coord
        self.observer = observer
        self.fill = fill
        self.color = color

def read_phaethon_report_csv():
    import csv
    fn_in = '20221021Phaethon_Report.csv'
    tbl = []
    with open(fn_in) as f:
        reader = csv.reader(f)
        tbl = [row for row in reader]
    mymarkers= []
    for (observer, str_lon, str_lat, status) in zip(tbl[4], tbl[9], tbl[10], 
                                                    tbl[17]):
        if observer == '':
            continue
        lat = coord_str_resolve(str_lat)
        lon = coord_str_resolve(str_lon)
        
        if lat != None and lon != None:
            stat = status_resolve(status)
            print(status)
            if stat == 'observed':
                fill = True
                color = '#ff0000'
            elif stat == 'pass':
                fill = False
                color = '#00ff00'
            else:
                fill = False
                color = '#0000ff'
            mk = MyMarker([lat, lon], observer, fill, color)
            mymarkers.append(mk)
    return mymarkers

def status_resolve(status):
    if status[0] == 'Y':
        return 'observed'
    elif status[0] == 'N':
        return 'pass'
    else:
        return 'failed'

def coord_str_resolve(coord_str):
    import re
    from astropy.coordinates import Angle
    import astropy.units as u

    pattern = '(\d+)\D+(\d+)\D+(\d+)*.'
    repattern = re.compile(pattern)
    result = repattern.match(coord_str)
    if result == None:
        coord = None
    else:
        coord_str = '{}d{}m{}s'.format(result.group(1), 
                                       result.group(2), result.group(3))
        # print(coord_str)  # Debug.
        try:
            coord = Angle(coord_str).to(u.deg).value
        except UnitsError:
            coord = None
    return coord


if __name__ == '__main__':
    fn_out = FN_OUT
    map = GsiMap()
    #coord = [43.19639, 141.44012]
    #popup = 'Akitaya'
    #map.add_marker(coord, popup)
    observers = read_phaethon_report_csv()
    #print(observers)
    map.add_markers(observers)
    map.save_html(fn_out)
    map.add_json('https://1601-031.a.hiroshima-u.ac.jp/~akitaya/gsimap_test/formatter.geojson')
