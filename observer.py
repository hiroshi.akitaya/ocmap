#!/usr/bin/env python3
#
#  Observer module
#    H. Akitaya (PERC/CIT)
#

class Observer(object):
    def __init__(self, site, name, coord, otype=None):
        self.site = site  # Site name.
        self.name = name  # Observer name.
        self.coord = coord  # Observer oordinates; (lng, lat)  [degrees]
        self.otype = otype  # Observer type; str.
