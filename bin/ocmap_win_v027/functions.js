
function get_gsi_elevation(lat, lon) {
    let url_gsi_api = 'https://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php';
    let url = url_gsi_api + '?lat=' + lat + '&lon=' + lon;

    const elev = document.getElementById('elevation');

    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            elev.textContent = `${data.elevation}` //  m (Source: ${data.hsrc})`;
        })
        .catch((e) => {
            console.log(e);
            } // Catch error.
        )
}